use criterion::async_executor::FuturesExecutor;
use criterion::*;

use yahoo_finance_api::Quote;

pub async fn test_future_join() -> (f64, f64, f64, (f64, f64)) {
    use futures::future::FutureExt;
    let adjclose = vec![
        1.2, 4.2, 6.4, 0.4, 1.2, 4.2, 6.4, 0.4, 1.2, 4.2, 6.4, 0.4, 1.2, 4.2, 6.4, 0.4, 1.2, 4.2,
        6.4, 0.4, 1.2, 4.2, 6.4, 0.4, 1.2, 4.2, 6.4, 0.4, 1.2, 4.2, 6.4, 0.4,
    ];
    let close = adjclose.clone();
    futures::join!(
        quotes_lib::min(&adjclose).map(|i| i.unwrap_or(0.0)),
        quotes_lib::max(&adjclose).map(|i| i.unwrap_or(0.0)),
        quotes_lib::last_sma(adjclose.len(), &adjclose).map(|f| f.unwrap_or(0.0)),
        quotes_lib::price_diff(&close).map(|i| i.unwrap_or((0.0, 0.0)))
    )
}

pub async fn test_future_await() -> (f64, f64, f64, (f64, f64)) {
    use futures::future::FutureExt;
    let adjclose = vec![
        1.2, 4.2, 6.4, 0.4, 1.2, 4.2, 6.4, 0.4, 1.2, 4.2, 6.4, 0.4, 1.2, 4.2, 6.4, 0.4, 1.2, 4.2,
        6.4, 0.4, 1.2, 4.2, 6.4, 0.4, 1.2, 4.2, 6.4, 0.4, 1.2, 4.2, 6.4, 0.4,
    ];
    let close = adjclose.clone();

    let min = quotes_lib::min(&adjclose).map(|i| i.unwrap_or(0.0)).await;
    let max = quotes_lib::max(&adjclose).map(|i| i.unwrap_or(0.0)).await;
    let last_sma = quotes_lib::last_sma(adjclose.len(), &adjclose)
        .map(|f| f.unwrap_or(0.0))
        .await;
    let abs_rel = quotes_lib::price_diff(&close)
        .map(|i| i.unwrap_or((0.0, 0.0)))
        .await;
    (min, max, last_sma, abs_rel)
}

pub fn bench_join(c: &mut Criterion) {
    c.bench_function("bench_join", move |b| {
        b.to_async(FuturesExecutor).iter(|| test_future_join())
    });
}

pub fn bench_await(c: &mut Criterion) {
    c.bench_function("bench_await", move |b| {
        b.to_async(FuturesExecutor).iter(|| test_future_await())
    });
}

pub fn bench_min(c: &mut Criterion) {
    c.bench_function("bench_min", move |b| {
        b.to_async(FuturesExecutor).iter(|| {
            quotes_lib::min(black_box(&[
                1.2, 4.2, 6.4, 0.4, 1.2, 4.2, 6.4, 0.4, 1.2, 4.2, 6.4, 0.4, 1.2, 4.2, 6.4, 0.4,
                1.2, 4.2, 6.4, 0.4, 1.2, 4.2, 6.4, 0.4, 1.2, 4.2, 6.4, 0.4, 1.2, 4.2, 6.4, 0.4,
            ]))
        })
    });
}

pub fn bench_max(c: &mut Criterion) {
    c.bench_function("bench_max", move |b| {
        b.to_async(FuturesExecutor).iter(|| {
            quotes_lib::max(black_box(&[
                1.2, 4.2, 6.4, 0.4, 1.2, 4.2, 6.4, 0.4, 1.2, 4.2, 6.4, 0.4, 1.2, 4.2, 6.4, 0.4,
                1.2, 4.2, 6.4, 0.4, 1.2, 4.2, 6.4, 0.4, 1.2, 4.2, 6.4, 0.4, 1.2, 4.2, 6.4, 0.4,
            ]))
        })
    });
}

pub fn bench_diff(c: &mut Criterion) {
    c.bench_function("bench_diff", move |b| {
        b.to_async(FuturesExecutor).iter(|| {
            quotes_lib::price_diff(black_box(&[
                1.2, 4.2, 6.4, 0.4, 1.2, 4.2, 6.4, 0.4, 1.2, 4.2, 6.4, 0.4, 1.2, 4.2, 6.4, 0.4,
                1.2, 4.2, 6.4, 0.4, 1.2, 4.2, 6.4, 0.4, 1.2, 4.2, 6.4, 0.4, 1.2, 4.2, 6.4, 0.4,
            ]))
        })
    });
}

pub fn bench_last_sma(c: &mut Criterion) {
    c.bench_function("bench_last_sma", move |b| {
        b.to_async(FuturesExecutor).iter(|| {
            quotes_lib::last_sma(
                black_box(32),
                black_box(&[
                    1.2, 4.2, 6.4, 0.4, 1.2, 4.2, 6.4, 0.4, 1.2, 4.2, 6.4, 0.4, 1.2, 4.2, 6.4, 0.4,
                    1.2, 4.2, 6.4, 0.4, 1.2, 4.2, 6.4, 0.4, 1.2, 4.2, 6.4, 0.4, 1.2, 4.2, 6.4, 0.4,
                ]),
            )
        })
    });
}

pub fn bench_csv(c: &mut Criterion) {
    let symbol = "GOOG".to_string();
    let quotes = vec![
        Quote {
            timestamp: 1641220200,
            open: 2889.510009765625,
            high: 2911.0,
            low: 2870.050048828125,
            volume: 1260700,
            close: 2901.489990234375,
            adjclose: 2901.489990234375,
        },
        Quote {
            timestamp: 1641306600,
            open: 2911.010009765625,
            high: 2932.199951171875,
            low: 2876.322998046875,
            volume: 1146400,
            close: 2888.330078125,
            adjclose: 2888.330078125,
        },
        Quote {
            timestamp: 1641393000,
            open: 2883.6201171875,
            high: 2885.9599609375,
            low: 2750.469970703125,
            volume: 2482100,
            close: 2753.070068359375,
            adjclose: 2753.070068359375,
        },
        Quote {
            timestamp: 1641479400,
            open: 2749.949951171875,
            high: 2793.719970703125,
            low: 2735.27001953125,
            volume: 1452500,
            close: 2751.02001953125,
            adjclose: 2751.02001953125,
        },
    ];

    c.bench_function("bench_csv", move |b| {
        b.to_async(FuturesExecutor)
            .iter(|| quotes_lib::csv_row_from(black_box(&symbol), black_box(&quotes)))
    });
}

criterion_group!(
    benches,
    bench_min,
    bench_max,
    bench_diff,
    bench_csv,
    bench_last_sma,
    bench_join,
    bench_await,
);
criterion_main!(benches);
