pub use futures;
pub use yahoo_finance_api as yahoo;

use std::sync::Arc;

use chrono::prelude::*;
use chrono::ParseResult;
use yahoo::{Quote, YahooConnector};
use yahoo::{YResponse, YahooError};

// ==== HELPERS ==================================================================

const DT_IN_FORMAT: &str = "%Y-%m-%d %H:%M:%S";
pub fn parse_dt_from_date(arg: &str) -> ParseResult<DateTime<Utc>> {
    let with_time = format!("{} 00:00:00", arg);
    Utc.datetime_from_str(with_time.as_str(), DT_IN_FORMAT)
}

pub const CSV_HEADER: &str = "period start,symbol,price,change %,min,max,30d avg\n";

pub async fn csv_row_from(symbol: &str, quotes: &[Quote]) -> String {
    use futures::future::FutureExt;

    let adjclose: Vec<_> = quotes.iter().map(|q| q.adjclose).collect();
    let last_quote = quotes.last().unwrap();

    let date = {
        let seconds = last_quote.timestamp as i64;
        let nanos = 0_u32;

        DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(seconds, nanos), Utc).to_rfc3339()
    };

    let min = crate::min(&adjclose).map(|i| i.unwrap_or(0.0)).await;
    let max = crate::max(&adjclose).map(|i| i.unwrap_or(0.0)).await;
    let last_sma = crate::last_sma(adjclose.len(), &adjclose)
        .map(|f| f.unwrap_or(0.0))
        .await;

    crate::price_diff(&quotes.iter().map(|q| q.close).collect::<Vec<f64>>())
        .map(|rel_opt| match rel_opt {
            Some((_, rel)) => {
                format!(
                    "{},{},${:.2},{:.2}%,${:.2},${:.2},${:.2}",
                    date, symbol, last_quote.close, rel, min, max, last_sma
                )
            }
            _ => format!("error create csv row for symbol {}", symbol),
        })
        .await
}

pub async fn quotes_for_symbol<'a>(
    provider: Arc<YahooConnector>,
    symbol: &'a str,
    from: Option<DateTime<Utc>>,
    to: Option<DateTime<Utc>>,
    range: Option<&'a str>,
    interval: &'a str,
) -> Result<(String, Vec<Quote>), String> {
    let handle = |response: Result<YResponse, YahooError>, smbl: &'a str| {
        response
            .and_then(|r| r.quotes().map(|qs| (smbl.to_string(), qs)))
            .map_err(|e| e.to_string())
    };

    match (from, to, range) {
        (Some(from), Some(to), _) => {
            let res = provider
                .get_quote_history_interval(symbol, from, to, interval)
                .await;
            handle(res, symbol)
        }
        (_, _, Some(range)) => {
            let res = provider.get_quote_range(symbol, interval, range).await;
            handle(res, symbol)
        }
        _ => {
            eprintln!("Invalid (missing) param: from | range");
            std::process::exit(1)
        }
    }
}

// ==== TO_IMPLEMENT =============================================================

///
/// # Returns
///
/// A tuple `(absolute, relative)` difference.
///
pub async fn price_diff(a: &[f64]) -> Option<(f64, f64)> {
    match a {
        [] => None,
        _ => match (a.first(), a.last()) {
            (Some(first_close), Some(last_close)) => {
                let abs = last_close - first_close;
                Some((abs, abs / first_close * 100.0))
            }
            _ => None,
        },
    }
}

///
/// Window function to create a simple moving average
///
pub async fn n_window_sma(n: usize, series: &[f64]) -> Option<Vec<f64>> {
    let window: Vec<_> = series.iter().copied().take(n).collect();
    if window.is_empty() {
        None
    } else {
        Some(window)
    }
}

///
/// Last SMA
///
pub async fn last_sma(n: usize, series: &[f64]) -> Option<f64> {
    use futures::future::FutureExt;
    n_window_sma(n, series)
        .map(|w| w.map(|v| v.iter().sum::<f64>() / n as f64))
        .await
}

///
/// Find the maximum in a series of f64
///
pub async fn max(series: &[f64]) -> Option<f64> {
    calc_extrema(series.iter().copied().to_owned().collect(), f64::max)
}

///
/// Find the minimum in a series of f64
///
pub async fn min(series: &[f64]) -> Option<f64> {
    calc_extrema(series.iter().copied().to_owned().collect(), f64::min)
}

fn calc_extrema<F: Fn(f64, f64) -> f64>(series: Vec<f64>, fun: F) -> Option<f64> {
    match series.as_slice() {
        [] => None,
        [a] => Some(*a),
        _ => Some(series[1..].iter().fold(series[0], |z, next| fun(z, *next))),
    }
}
