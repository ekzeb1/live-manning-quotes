use actix::prelude::*;
use live_manning_quotes::actors::*;
use std::sync::Arc;
use yahoo_finance_api::{Quote, YahooConnector};

#[test]
fn test_processor_csv_response() {
    let qs: Quotes = Quotes {
        symbol: "GOOG".to_string(),
        quotes: vec![
            Quote {
                timestamp: 1641220200,
                open: 2889.510009765625,
                high: 2911.0,
                low: 2870.050048828125,
                volume: 1260700,
                close: 2901.489990234375,
                adjclose: 2901.489990234375,
            },
            Quote {
                timestamp: 1641306600,
                open: 2911.010009765625,
                high: 2932.199951171875,
                low: 2876.322998046875,
                volume: 1146400,
                close: 2888.330078125,
                adjclose: 2888.330078125,
            },
            Quote {
                timestamp: 1641393000,
                open: 2883.6201171875,
                high: 2885.9599609375,
                low: 2750.469970703125,
                volume: 2482100,
                close: 2753.070068359375,
                adjclose: 2753.070068359375,
            },
            Quote {
                timestamp: 1641479400,
                open: 2749.949951171875,
                high: 2793.719970703125,
                low: 2735.27001953125,
                volume: 1452500,
                close: 2751.02001953125,
                adjclose: 2751.02001953125,
            },
        ],
    };

    let succ_resp = qs.csv();

    System::new().block_on(async {
        let addr = StockDataProcessor.start();
        let res = addr.send(qs.clone()).await.unwrap();
        assert!(res.is_ok());
        assert_eq!(succ_resp, res.unwrap());
    })
}

#[test]
fn test_loader_csv_response() {
    let req = QuoteRequest::from("GOOG", "2022-01-30", "2022-02-02");

    let succ_resp = "2022-02-01 14:30:00+00:00,GOOG,$2757.57,1.61%,$2713.97,$2757.57";

    System::new().block_on(async {
        let provider = Arc::new(YahooConnector::new());
        let addr = StockDataDownloader::from(provider, 30).start();
        let res = addr.send(req).await.unwrap();
        System::current().stop();
        assert!(res.is_ok());
        assert_eq!(succ_resp, res.unwrap());
    })
}
