live-manning-quotes
===================

 ```bash
$ cargo test

$ cargo bench

$ cargo run -p quotes-cli -- --from=2022-01-23
 
$ cargo run -p quotes-cli --example load_sp500
```

### Cross linux-musl on MacOS darwin
```bash
$ brew tap SergioBenitez/osxct
 
$ brew install FiloSottile/musl-cross/musl-cross

$ rustup target add x86_64-unknown-linux-musl

$ brew install binutils

$ chmod +x ./scripts/build-lib-musl.sh && ./scripts/build-lib-musl.sh
```

