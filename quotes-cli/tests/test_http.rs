use actix::{Actor, Addr};

use quotes_cli::store::{BufferSink, PerformanceIndicators};
use reqwest::Client;
use std::collections::VecDeque;
use std::net::TcpListener;

type ServerAddr = String;

pub struct TestApp {
    pub address: ServerAddr,
    pub buf_sink_addr: Addr<BufferSink>,
    pub buf_sink: BufferSink,
}

pub async fn spawn_app() -> TestApp {
    let listener = TcpListener::bind("127.0.0.1:0").expect("Failed to bind random port");

    // We retrieve the port assigned to us by the OS
    let port = listener.local_addr().unwrap().port();

    // We return the application address to the caller!
    let address = format!("http://127.0.0.1:{}", port);

    let mut data_sink = VecDeque::with_capacity(11);

    data_sink.push_back(PerformanceIndicators::default());

    let buf_sink_act = BufferSink::with_sink_data(1, data_sink);
    let buf_sink = buf_sink_act.clone();
    let buf_sink_addr = buf_sink_act.start();

    // set mock dn name as uuid

    let server =
        quotes_cli::http::run(listener, buf_sink_addr.clone()).expect("Failed to bind address");
    // Launch the server as a background task
    // tokio::spawn returns a handle to the spawned future,
    // but we have no use for it here, hence the non-binding let
    //
    // New dev dependency - let's add tokio to the party with
    // `cargo add tokio --dev --vers 1`
    let _ = tokio::spawn(server);

    TestApp {
        address,
        buf_sink_addr,
        buf_sink,
    }
}

#[actix_rt::test]
async fn health_check_works() {
    let app = spawn_app().await;

    // We need to bring in `reqwest`
    // to perform HTTP requests against our application.
    //
    // Use `cargo add reqwest --dev --vers 0.11` to add
    // it under `[dev-dependencies]` in Cargo.toml
    let client = Client::new();

    let response = client
        .get(&format!("{}/tail/0", app.address))
        .send()
        .await
        .expect("Failed execute health_check req");

    assert_eq!(
        app.buf_sink.data_sink.read().unwrap()[0].csv_row(),
        response.json::<Vec<PerformanceIndicators>>().await.unwrap()[0].csv_row()
    );
    //assert_eq!(Some(0), response.content_length());
}
