use actix::prelude::*;

use quotes_cli::load::{QuoteRequest, Quotes, StockDataDownloader};
use quotes_cli::process::StockDataProcessor;
use quotes_lib::yahoo::{Quote, YahooConnector};
use std::sync::Arc;

#[test]
fn test_processor_csv_response() {
    let qs: Quotes = Quotes {
        symbol: "GOOG".to_string(),
        quotes: vec![
            Quote {
                timestamp: 1641220200,
                open: 2889.510009765625,
                high: 2911.0,
                low: 2870.050048828125,
                volume: 1260700,
                close: 2901.489990234375,
                adjclose: 2901.489990234375,
            },
            Quote {
                timestamp: 1641306600,
                open: 2911.010009765625,
                high: 2932.199951171875,
                low: 2876.322998046875,
                volume: 1146400,
                close: 2888.330078125,
                adjclose: 2888.330078125,
            },
            Quote {
                timestamp: 1641393000,
                open: 2883.6201171875,
                high: 2885.9599609375,
                low: 2750.469970703125,
                volume: 2482100,
                close: 2753.070068359375,
                adjclose: 2753.070068359375,
            },
            Quote {
                timestamp: 1641479400,
                open: 2749.949951171875,
                high: 2793.719970703125,
                low: 2735.27001953125,
                volume: 1452500,
                close: 2751.02001953125,
                adjclose: 2751.02001953125,
            },
        ],
    };

    System::new().block_on(async {
        let succ_resp = qs.csv().await;
        let addr = StockDataProcessor.start();
        let res = addr.send(qs.clone()).await.unwrap();
        assert!(res.is_ok());
        assert_eq!(succ_resp, res.unwrap().csv_row());
    })
}

#[test]
fn test_loader_csv_response() {
    let req = QuoteRequest::from("GOOG", Some("2022-01-30"), Some("2022-02-02"), None, "1d");

    System::new().block_on(async {
        let provider = Arc::new(YahooConnector::new());
        let loader = StockDataDownloader::from(provider, 30);
        let req_clone = req.clone();
        let qsr = quotes_lib::quotes_for_symbol(
            loader.provider.clone(),
            &req_clone.symbol,
            req.from,
            req.to,
            None,
            "1d",
        )
        .await
        .map(|(s, qs)| Quotes {
            symbol: s.to_string(),
            quotes: qs,
        });

        assert!(qsr.is_ok());

        let qs = qsr.unwrap();
        let proc_succ_resp = Ok(qs.clone().csv().await);
        let proc_res = loader.processor.send(qs).await.unwrap();

        assert_eq!(proc_succ_resp, proc_res.map(|v| v.csv_row()));

        let addr = loader.start();
        let res = addr.send(req).await.unwrap();
        System::current().stop();

        assert!(res.is_ok());

        assert_eq!(proc_succ_resp, res.map(|v| v.csv_row()));
    })
}
