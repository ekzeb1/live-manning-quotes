#![allow(unused_variables)]
pub mod actors;
pub mod http;

pub use crate::actors::load;
pub use crate::actors::process;
pub use crate::actors::store;

pub use actix;
pub use chrono;
pub use futures;
pub use serde;

use actix::prelude::*;
use actix_web::dev::Server;
use actors::file::FileSink;
use actors::load::{QuoteRequest, StockDataDownloader};
use actors::store::BufferSink;
use chrono::prelude::*;
use chrono::Duration;
use clap::{App, Arg, ArgMatches};
use futures::stream::{self, Stream, StreamExt};
use quotes_lib::yahoo::{Quote, YahooConnector};
use regex::Regex;
use std::future::Future;
use std::net::TcpListener;
use std::sync::Arc;

pub struct QuotesApp {
    provider: Arc<YahooConnector>,
    symbols: Vec<String>,
    from: Option<DateTime<Utc>>,
    range: Option<String>,
    interval: String,
    to: Option<DateTime<Utc>>,
    update_interval: u64,
    store_buf_size: usize,
    filename: Option<String>,
    application_port: u16,
}

impl Default for QuotesApp {
    fn default() -> Self {
        Self::new()
    }
}

impl QuotesApp {
    pub fn new() -> QuotesApp {
        QuotesApp::from(Self::app_args(), vec![])
    }

    pub fn from(matches: ArgMatches, symbols: Vec<String>) -> Self {
        let provider = Arc::new(YahooConnector::new());
        let from = matches
            .value_of("from")
            .and_then(|arg| quotes_lib::parse_dt_from_date(arg).ok());
        let symbols = if symbols.is_empty() {
            matches
                .value_of("symbols")
                .map(|s| s.split(',').map(|s| s.to_string()))
                .unwrap()
                .collect()
        } else {
            symbols
        };

        let to = matches
            .value_of("to")
            .map(|arg| match quotes_lib::parse_dt_from_date(arg) {
                Ok(dt) => dt,
                Err(e) => panic!("Error parse param 'to' {}", e),
            });

        let range = matches.value_of("range").map(|s| s.to_string());
        let interval = matches.value_of("interval").unwrap().to_string();
        let update_interval = matches
            .value_of("update-interval")
            .map(|un| match un.parse::<u64>() {
                Ok(result) => result,
                Err(err) => panic!("Error parse `update-interval` param {}", err),
            })
            .unwrap();

        let store_buf_size = matches
            .value_of("store_buf_size")
            .map(|size| match size.parse::<usize>() {
                Ok(size) if symbols.len() > size && symbols.len() <= 50 => {
                    eprintln!(
                        "store_buf_size` param {} <= then {}. set to symbols num * 2",
                        size,
                        symbols.len()
                    );
                    symbols.len() * 2
                }
                Ok(size) if symbols.len() > size && symbols.len() > 50 => {
                    eprintln!(
                        "store_buf_size` param {} > then {}. set to symbols num",
                        size,
                        symbols.len()
                    );
                    symbols.len()
                }
                Ok(size) => size,
                Err(err) => {
                    eprintln!(
                        "Error parse `qstore_buf_size` param {} set to symbols num * 2",
                        err
                    );
                    symbols.len() * 2
                }
            })
            .unwrap();

        let filename = matches.value_of("filename").map(|s| s.to_string());

        let application_port = matches
            .value_of("application_port")
            .map(|port| match port.parse::<u16>() {
                Ok(port) => port,
                Err(e) => {
                    eprintln!(
                        "Parse `application_port` param error: {}. Set default 3000",
                        e
                    );
                    3000
                }
            })
            .unwrap();

        QuotesApp {
            provider,
            symbols,
            from,
            range,
            interval,
            to,
            update_interval,
            store_buf_size,
            filename,
            application_port,
        }
    }

    pub async fn start(self) -> Result<Server, std::io::Error> {
        let from = self.from;
        let to = self.to;
        let range = self.range;
        let interval = self.interval;

        let buf_sink = BufferSink::new(self.store_buf_size).start();

        let _: Option<Addr<FileSink>> = self
            .filename
            .map(|file| FileSink::new(file, self.symbols.len()).start());

        for symbol in self.symbols {
            let provider = self.provider.clone();
            let range = range.clone();
            let interval = interval.clone();
            actix::spawn(async move {
                let downloader = StockDataDownloader::from(provider, self.update_interval).start();
                let req = QuoteRequest {
                    symbol,
                    from,
                    to,
                    range,
                    interval,
                };
                downloader.do_send(req);
            });
        }

        let address = format!("0.0.0.0:{}", self.application_port);
        println!("\nStaring server on: {}...", address);
        let listener = TcpListener::bind(address).expect("Failed bind to 8000");

        crate::http::run(listener, buf_sink)
    }

    // ===== INFO_FETCHERS ===================================================

    fn quotes_for_symbol<'a>(
        &'a self,
        symbol: &'a str,
    ) -> impl Future<Output = (String, Vec<Quote>)> + 'a {
        use futures::future::FutureExt;

        quotes_lib::quotes_for_symbol(
            self.provider.clone(),
            symbol,
            self.from,
            self.to,
            self.range.as_deref(),
            &self.interval,
        )
        .map(move |r| r.unwrap_or_else(|_| (symbol.to_string(), vec![])))
    }

    pub fn quotes(&self) -> impl Stream<Item = (String, Vec<Quote>)> + '_ {
        stream::iter(&self.symbols)
            .map(|symbol| self.quotes_for_symbol(symbol))
            .buffer_unordered(self.symbols.len())
    }

    // ===== HELPERS =========================================================

    pub async fn csv(&self) -> Result<String, String> {
        let qs: Vec<_> = self
            .quotes()
            .map(|(symbol, quotes)| async move { quotes_lib::csv_row_from(&symbol, &quotes).await })
            .buffered(16)
            .collect()
            .await;

        let csv: Vec<_> = vec![quotes_lib::CSV_HEADER]
            .into_iter()
            .chain(qs.iter().map(|s| s.as_str()))
            .collect();

        Ok(csv.join("\n"))
    }

    pub fn app_args() -> ArgMatches {
        let from_dt_regex = Regex::new(r"^\d{4}-\d{2}-\d{2}$").unwrap();
        let port_regex = Regex::new(r"^\d{1,5}$").unwrap();
        let now = Utc::now();
        let default_to = now.format("%Y-%m-%d").to_string();
        let default_from = (now - Duration::days(30)).format("%Y-%m-%d").to_string();
        App::new("live-manning-quotes")
            .version("0.1.0")
            .arg(
                Arg::new("symbols")
                    .help_heading("Stock symbol names eg AAPL or AAPL,MSFT,UBER,GOOG")
                    .default_value("AAPL,MSFT,UBER,GOOG"),
            )
            .arg(
                Arg::new("from")
                    .validator_regex(&from_dt_regex, "Invalid date format: YYYY-mm-dd required")
                    .long("from")
                    .default_value(default_from.as_str())
                    .help_heading("Stocks history from date: 2022-01-22"),
            )
            .arg(
                Arg::new("to")
                    .validator_regex(&from_dt_regex, "Invalid date format: YYYY-mm-dd required")
                    .long("to")
                    .default_value(default_to.as_str())
                    .help_heading("Stocks history to date: 2022-01-22"),
            )
            .arg(
                Arg::new("interval")
                    .long("interval")
                    .short('i')
                    .help_heading("Interval eg 1d")
                    .default_value("1d"),
            )
            .arg(
                Arg::new("range")
                    .long("range")
                    .short('r')
                    .required_unless_present("from")
                    .help_heading("Range eg 1d")
                    .default_value("1d"),
            )
            .arg(
                Arg::new("update-interval")
                    .long("update-interval")
                    .short('u')
                    .help_heading("Update quotes info interval")
                    .default_value("30"),
            )
            .arg(
                Arg::new("store_buf_size")
                    .long("store_buf_size")
                    .short('s')
                    .help_heading("Number of last quotes hold in mem. Default: quotes number * 2")
                    .default_value("0"),
            )
            .arg(
                Arg::new("filename")
                    .long("filename")
                    .short('f')
                    .help_heading("File name to persis csv. Default: quotes.csv")
                    .default_value("quotes.csv"),
            )
            .arg(
                Arg::new("application_port")
                    .validator_regex(&port_regex, "Invalid port param: only not <= 5 digits")
                    .long("port")
                    .short('p')
                    .help_heading("Application http port. Default: 3000")
                    .default_value("3000"),
            )
            .get_matches()
    }
}
