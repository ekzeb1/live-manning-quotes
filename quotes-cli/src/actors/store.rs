use crate::actors::load::Quotes;
use actix::prelude::*;
use actix_broker::{BrokerSubscribe, SystemBroker};
use chrono::{DateTime, NaiveDateTime, Utc};
use quotes_lib::yahoo::Quote;
use serde::{Deserialize, Serialize};
use std::collections::VecDeque;
use std::sync::{Arc, RwLock};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct PerformanceIndicators {
    pub(crate) timestamp: DateTime<Utc>,
    pub(crate) symbol: String,
    pub(crate) price: f64,      // close price
    pub(crate) pct_change: f64, // * 100.0, // rel price change %
    pub(crate) period_min: f64,
    pub(crate) period_max: f64,
    pub(crate) last_sma: f64,
}

impl Default for PerformanceIndicators {
    fn default() -> Self {
        PerformanceIndicators {
            timestamp: Utc::now(),
            symbol: String::new(),
            price: -0.0,
            pct_change: -0.0,
            period_min: -0.0,
            period_max: -0.0,
            last_sma: -0.0,
        }
    }
}

impl PerformanceIndicators {
    pub async fn from(symbol: String, quotes: Vec<Quote>) -> Result<PerformanceIndicators, String> {
        use futures::future::FutureExt;

        if quotes.is_empty() {
            return Err(format!("quotes for symbol {} is empty", symbol));
        }

        let close = quotes.iter().map(|q| q.close).collect::<Vec<_>>();
        let adjclose = quotes.iter().map(|q| q.adjclose).collect::<Vec<_>>();
        let last_quote = quotes.last().unwrap();
        let price = last_quote.close;

        let timestamp = {
            let seconds = last_quote.timestamp as i64;
            let nanos = 0_u32;

            DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(seconds, nanos), Utc)
        };

        let period_min = quotes_lib::min(&adjclose).map(|i| i.unwrap_or(0.0)).await;
        let period_max = quotes_lib::max(&adjclose).map(|i| i.unwrap_or(0.0)).await;
        let last_sma = quotes_lib::last_sma(adjclose.len(), &adjclose)
            .map(|f| f.unwrap_or(0.0))
            .await;
        let (_, pct_change) = quotes_lib::price_diff(&close)
            .map(|i| i.unwrap_or((0.0, 0.0)))
            .await;

        Ok(PerformanceIndicators {
            timestamp,
            symbol,
            price,
            pct_change,
            period_min,
            period_max,
            last_sma,
        })
    }

    pub async fn from_quotes(qs: Quotes) -> Result<PerformanceIndicators, String> {
        let Quotes { symbol, quotes } = qs;

        Self::from(symbol, quotes).await
    }

    pub fn csv_row(&self) -> String {
        format!(
            "{},{},${:.2},{:.2}%,${:.2},${:.2},${:.2}",
            self.timestamp.to_rfc3339(),
            self.symbol,
            self.price,
            self.pct_change,
            self.period_min,
            self.period_max,
            self.last_sma
        )
    }
}

impl Message for PerformanceIndicators {
    type Result = ();
}

type DataSink = Arc<RwLock<VecDeque<PerformanceIndicators>>>;

///
/// Actor that saves incoming messages to a ring buffer
///
#[derive(Default, Debug, Clone)]
pub struct BufferSink {
    pub data_sink: DataSink,
    dec_size: usize,
}

impl BufferSink {
    pub fn new(dec_size: usize) -> Self {
        Self {
            data_sink: Arc::new(RwLock::new(VecDeque::new())),
            dec_size,
        }
    }

    pub fn with_sink_data(dec_size: usize, data_sink: VecDeque<PerformanceIndicators>) -> Self {
        Self {
            data_sink: Arc::new(RwLock::new(data_sink)),
            dec_size,
        }
    }
}

impl Actor for BufferSink {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        self.subscribe_async::<SystemBroker, PerformanceIndicators>(ctx);
    }
}

impl Handler<PerformanceIndicators> for BufferSink {
    type Result = ();

    fn handle(&mut self, msg: PerformanceIndicators, _ctx: &mut Self::Context) -> Self::Result {
        let mut sink = self.data_sink.write().unwrap();
        sink.push_back(msg);
        if sink.len() > self.dec_size {
            sink.swap_remove_front(0);
        }
    }
}

pub struct BufTail(pub usize);

impl Message for BufTail {
    type Result = Result<VecDeque<PerformanceIndicators>, String>;
}

impl Handler<BufTail> for BufferSink {
    type Result = Result<VecDeque<PerformanceIndicators>, String>;

    fn handle(&mut self, msg: BufTail, _ctx: &mut Self::Context) -> Self::Result {
        let dec: VecDeque<_> = self.data_sink.clone().read().unwrap().clone();
        Ok(match msg.0 {
            0 => dec,
            n => dec.into_iter().take(n).collect(),
        })
    }
}
