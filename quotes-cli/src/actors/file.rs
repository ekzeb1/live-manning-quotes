use std::fs::File;
use std::io::{BufWriter, Write};

use actix::prelude::*;
use actix_broker::{BrokerSubscribe, SystemBroker};

use crate::load::Stop;
use crate::store::PerformanceIndicators;

///
/// Actor for storing incoming messages in a csv file
///
#[derive(Default)]
pub struct FileSink {
    #[allow(dead_code)]
    filename: String,
    writer: Option<BufWriter<File>>,
    symbols_num: usize,
    written: usize,
}

impl FileSink {
    pub fn new(filename: String, symbols_num: usize) -> Self {
        match File::create(&filename) {
            Ok(file) => Self {
                filename,
                writer: Some(BufWriter::new(file)),
                symbols_num,
                written: 0,
            },
            _ => Self {
                filename,
                writer: None,
                symbols_num,
                written: 0,
            },
        }
    }
}

impl Actor for FileSink {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        self.subscribe_async::<SystemBroker, PerformanceIndicators>(ctx);
        self.subscribe_async::<SystemBroker, Stop>(ctx);
    }

    fn stopped(&mut self, _ctx: &mut Self::Context) {
        if let Some(writer) = &mut self.writer {
            writer.flush().unwrap();
            actix::System::current().stop();
            std::process::exit(1)
        }
    }
}

impl Handler<PerformanceIndicators> for FileSink {
    type Result = ();

    fn handle(&mut self, msg: PerformanceIndicators, _ctx: &mut Self::Context) -> Self::Result {
        if let Some(file) = &mut self.writer {
            if self.written == 0 {
                file.write_all(quotes_lib::CSV_HEADER.as_bytes()).unwrap()
            }
            file.write_all(
                format!(
                    "{},{},${:.2},{:.2}%,${:.2},${:.2},${:.2}\n",
                    msg.timestamp.to_rfc3339(),
                    msg.symbol,
                    msg.price,
                    msg.pct_change * 100.0,
                    msg.period_min,
                    msg.period_max,
                    msg.last_sma
                )
                .as_bytes(),
            )
            .unwrap();
            self.written = self.written.overflowing_add(1).0;
            if self.written % self.symbols_num == 0 {
                file.flush().unwrap();
            }
        }
    }
}

impl Handler<Stop> for FileSink {
    type Result = ();

    fn handle(&mut self, _msg: Stop, ctx: &mut Self::Context) -> Self::Result {
        self.symbols_num -= 1;
        if self.symbols_num == 0 {
            eprintln!("stop further downloads due all requests returned errors, stop the app");
            ctx.stop()
        }
    }
}
