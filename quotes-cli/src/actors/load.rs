use crate::process::StockDataProcessor;
use crate::store::PerformanceIndicators;
use actix::prelude::*;
use actix_broker::{BrokerIssue, SystemBroker};
use chrono::prelude::*;
use quotes_lib::yahoo::{self, YahooConnector};
use std::pin::Pin;
use std::sync::Arc;
use std::time::Duration;

#[derive(Debug, Default, Clone)]
pub struct Quotes {
    pub symbol: String,
    pub quotes: Vec<yahoo::Quote>,
}

impl Message for Quotes {
    type Result = Result<PerformanceIndicators, String>;
}

impl Quotes {
    pub fn new(symbol: String) -> Self {
        Self {
            symbol,
            quotes: vec![],
        }
    }

    pub async fn csv(&self) -> String {
        quotes_lib::csv_row_from(&self.symbol, &self.quotes).await
    }
}

#[derive(Debug, Clone)]
pub struct QuoteRequest {
    pub symbol: String,
    pub from: Option<DateTime<Utc>>,
    pub to: Option<DateTime<Utc>>,
    pub range: Option<String>,
    pub interval: String,
}

impl QuoteRequest {
    pub fn from(
        symbol: &str,
        from_dt: Option<&str>,
        to_dt: Option<&str>,
        range: Option<&str>,
        interval: &str,
    ) -> Self {
        let from = from_dt.map(|from_dt| quotes_lib::parse_dt_from_date(from_dt).unwrap());
        let to = to_dt.map(|to_dt| quotes_lib::parse_dt_from_date(to_dt).unwrap());
        let symbol = symbol.to_string();
        let range = range.map(|r| r.to_string());
        let interval = interval.to_string();

        QuoteRequest {
            symbol,
            from,
            to,
            range,
            interval,
        }
    }
}

impl Message for QuoteRequest {
    type Result = Result<PerformanceIndicators, String>;
}

pub struct StockDataDownloader {
    req: Option<QuoteRequest>,
    pub processor: Addr<StockDataProcessor>,
    pub provider: Arc<YahooConnector>,
    interval_sec: u64,
}

impl StockDataDownloader {
    pub fn send_timer(&mut self, ctx: &mut Context<Self>) {
        ctx.address().do_send(Timer)
    }

    pub fn from(provider: Arc<YahooConnector>, interval_sec: u64) -> Self {
        Self {
            req: None,
            processor: StockDataProcessor.start(),
            provider,
            interval_sec,
        }
    }

    pub fn download(
        &self,
        req: QuoteRequest,
        self_addr: Addr<StockDataDownloader>,
    ) -> Pin<Box<dyn futures::Future<Output = Result<PerformanceIndicators, String>>>> {
        let processor = self.processor.clone();
        let provider = self.provider.clone();

        Box::pin(async move {
            let resp = quotes_lib::quotes_for_symbol(
                provider,
                &req.symbol,
                req.from,
                req.to,
                req.range.as_deref(),
                &req.interval,
            )
            .await;

            match resp {
                Ok((symbol, quotes)) => processor
                    .send(Quotes {
                        symbol: symbol.to_string(),
                        quotes,
                    })
                    .await
                    .unwrap(),
                Err(err) => {
                    eprintln!("{}", err);
                    self_addr.do_send(Stop);
                    Err(err)
                }
            }
        })
    }
}

impl Actor for StockDataDownloader {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        ctx.run_interval(Duration::from_secs(self.interval_sec), Self::send_timer);
    }
}

pub struct Timer;

impl Message for Timer {
    type Result = Result<PerformanceIndicators, String>;
}

impl Handler<Timer> for StockDataDownloader {
    type Result = ResponseActFuture<Self, Result<PerformanceIndicators, String>>;

    fn handle(&mut self, _: Timer, ctx: &mut Self::Context) -> Self::Result {
        let mut req = self.req.clone().unwrap();
        req.to = Some(Utc::now());
        let self_addr = ctx.address();
        self.download(req, self_addr)
            .into_actor(self)
            .map_err(|err, act, _ctx| {
                act.issue_async::<SystemBroker, _>(Stop);
                err
            })
            .boxed_local()
    }
}

#[derive(Clone, Debug)]
pub struct Stop;

impl Message for Stop {
    type Result = ();
}

impl Handler<Stop> for StockDataDownloader {
    type Result = ();

    fn handle(&mut self, _msg: Stop, ctx: &mut Self::Context) -> Self::Result {
        let symbol = self.req.clone().unwrap().symbol;
        eprintln!(
            "stop further downloads for symbol {} due failed quotes request.",
            symbol,
        );
        ctx.stop()
    }
}

impl Handler<QuoteRequest> for StockDataDownloader {
    type Result = ResponseActFuture<Self, Result<PerformanceIndicators, String>>;

    fn handle(&mut self, req: QuoteRequest, ctx: &mut Self::Context) -> Self::Result {
        self.req = Some(req.clone());
        let self_addr = ctx.address();
        self.download(req, self_addr)
            .into_actor(self)
            .map_err(|err, act, _ctx| {
                act.issue_async::<SystemBroker, _>(Stop);
                err
            })
            .boxed_local()
    }
}
