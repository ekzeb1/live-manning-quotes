use crate::load::Quotes;
use crate::store::PerformanceIndicators;
use actix::prelude::*;
use actix_broker::{BrokerIssue, SystemBroker};

pub struct StockDataProcessor;

impl Actor for StockDataProcessor {
    type Context = Context<Self>;
}

impl Handler<Quotes> for StockDataProcessor {
    type Result = ResponseActFuture<Self, Result<PerformanceIndicators, String>>;

    fn handle(&mut self, msg: Quotes, _ctx: &mut Self::Context) -> Self::Result {
        PerformanceIndicators::from_quotes(msg)
            .into_actor(self)
            .map(|res, act, _ctx| {
                let indicators = res.unwrap();
                act.issue_async::<SystemBroker, _>(indicators.clone());
                println!("{}", indicators.csv_row());
                Ok(indicators)
            })
            .boxed_local()
    }
}
