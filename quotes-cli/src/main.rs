use actix_web::dev::Server;
use quotes_cli::QuotesApp;

#[actix_web::main]
async fn main() -> Result<(), std::io::Error> {
    // - async without actors - //
    // QuotesApp::new().csv().await.map(|csv| println!("{}", csv));

    let server: Server = QuotesApp::new().start().await?;

    println!("\nServer started: 🎩 press Ctrl-C to stop");
    println!("=========================================\n");
    print!("{}", quotes_lib::CSV_HEADER);
    server.await?;

    println!("🎩 Ctrl-C received, stopping server");
    Ok(())
}
