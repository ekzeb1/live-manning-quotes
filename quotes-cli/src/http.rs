use actix::Addr;
use actix_web::dev::Server;
use actix_web::web::{self, Data, Json, Path};
use actix_web::{error, App, HttpServer, Responder, Result};

use futures::future::FutureExt;

use crate::store::BufTail;
use crate::BufferSink;
use std::net::TcpListener;

pub async fn tail(number: Path<u32>, storage: Data<Addr<BufferSink>>) -> Result<impl Responder> {
    storage
        .clone()
        .send(BufTail(number.into_inner() as usize))
        .map(|r| match r.unwrap() {
            Ok(dec) => Ok(Json(dec)),
            Err(err) => Err(error::ErrorInternalServerError(err)),
        })
        .await
}

pub fn run(listener: TcpListener, buf_sink: Addr<BufferSink>) -> Result<Server, std::io::Error> {
    let data = web::Data::new(buf_sink);
    let server = HttpServer::new(move || {
        App::new()
            .route("/tail/{number}", web::get().to(tail))
            .app_data(data.clone())
    })
    .shutdown_timeout(1000)
    .listen(listener)?
    .run();
    Ok(server)
}
