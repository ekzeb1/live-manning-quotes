use quotes_cli::QuotesApp;

#[actix_web::main]
async fn main() -> Result<(), std::io::Error> {
    let args = QuotesApp::app_args();

    let symbols = std::fs::read_to_string("./var/sp500.txt")
        .map(|s| s.split(',').map(|l| l.to_string()).collect::<Vec<_>>())
        .unwrap();

    let server = QuotesApp::from(args, symbols).start().await?;

    println!("Server started");
    print!("{}", quotes_lib::CSV_HEADER);
    server.await?;

    println!("🎩 Ctrl-C received, stopping server");
    Ok(())
}
