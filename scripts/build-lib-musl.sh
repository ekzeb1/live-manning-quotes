#!/usr/bin/env bash

export TARGET_CC=x86_64-linux-musl-gcc
export RUSTFLAGS="-C linker=x86_64-linux-musl-gcc"

cargo build -p quotes-lib --release --target=x86_64-unknown-linux-musl